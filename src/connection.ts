import { debug, info, warn } from '@kominal/observer-node-client';
import { toBuffer, toString } from 'ip';
import { Socket } from 'net';
import { updateSessions } from '.';
import { shiftLeft } from './helper/helper';
import { AddressType } from './models/addressType';
import { Session } from './models/session';

export const connections: Connection[] = [];

export class Connection {
	public session: Session | undefined;
	private handshakeCompleted = false;

	constructor(private source: Socket) {
		debug(`Received connection from ${source.remoteAddress}...`);

		connections.push(this);
		updateSessions();

		source.on('close', (e) => this.close());
		source.on('end', () => this.close());
		source.on('error', (e) => this.close());

		const listener = (data: Buffer) => {
			this.handleHandshake(data, listener);
		};

		source.on('data', listener);
	}

	handleHandshake(data: Buffer, listener: any) {
		try {
			if (!this.session && this.handleMaskConfiguration(data)) {
				return;
			}

			if (shiftLeft(data, 1).readUInt8() !== 5) {
				throw new Error(`Received unsupported SOCKS version from ${this}`);
			}

			if (!this.handshakeCompleted) {
				this.handleSocksConfiguration(data);
				this.handshakeCompleted = true;
				return;
			}

			if (shiftLeft(data, 1).readUInt8() !== 1) {
				throw new Error(`Received unsupported SOCKS command from ${this}`);
			}

			this.source.removeListener('data', listener);

			shiftLeft(data, 1); //Remove reserved bit

			this.connect(this.determineTargetAdress(data), shiftLeft(data, 2).readUInt16BE());
		} catch (e) {
			warn(`Error during SOCKS handshake: ${e}`);
			this.source.end();
		}
	}

	handleMaskConfiguration(data: Buffer): boolean {
		const index = data.indexOf(0x7d) + 1;
		if (index <= 0) {
			throw new Error(`Did not receive mask configuration from ${this}`);
		}

		this.session = JSON.parse(data.subarray(0, index).toString('utf-8'));
		info(`Client requested mask(${this.session?.maskIp})`);

		if (index >= data.length) {
			return true;
		}
		shiftLeft(data, index);
		return false;
	}

	handleSocksConfiguration(data: Buffer) {
		const methods = shiftLeft(data, shiftLeft(data, 1).readUInt8());

		if (!methods.includes(0x00)) {
			this.send(Buffer.from([0xff]));
			throw new Error(`Received unsupported SOCKS method from ${this}`);
		}

		this.send(Buffer.from([0x00]));
	}

	determineTargetAdress(data: Buffer) {
		switch (shiftLeft(data, 1).readUInt8()) {
			case AddressType.IPV4:
				return toString(shiftLeft(data, 4));
			case AddressType.DOMAIN:
				return shiftLeft(data, shiftLeft(data, 1).readUInt8()).toString('utf-8');
			case AddressType.IPV6:
				return toString(shiftLeft(data, 16));
		}
		throw new Error(`Received unsupported SOCKS destination address from ${this}`);
	}

	connect(targetAddress: string, targetPort: number) {
		this.source.pause();

		const target = new Socket();
		target.on('connect', () => {
			const localAddress = toBuffer(target.localAddress);

			const response = Buffer.alloc(6 + localAddress.length);
			Buffer.from([0x05, 0x00, 0x00, localAddress.length === 4 ? 0x01 : 0x04]).copy(response);
			localAddress.copy(response, 4);
			response.writeUInt16BE(target.localPort, 4 + localAddress.length);

			this.source.write(response);
			this.source.pipe(target);
			target.pipe(this.source);
			this.source.resume();
			debug(`Connected client from ${this.source.remoteAddress} to ${targetAddress}:${targetPort}.`);
		});
		target.connect({
			port: targetPort,
			host: targetAddress,
			localAddress: this.session?.maskIp,
			family: this.session?.family,
		});

		this.source.on('close', (e) => target.end());
		target.on('close', (e) => this.source.end());
		this.source.on('end', () => target.end());
		target.on('end', () => this.source.end());
		this.source.on('error', (e) => {
			warn(`Client error on ${this.session?.group}/${this.session?.identifier}/${this.session?.family} via ${this.session?.maskIp}: ${e}`);
			target.end();
		});
		target.on('error', (e) => {
			warn(`Data error on ${this.session?.group}/${this.session?.identifier}/${this.session?.family} via ${this.session?.maskIp}: ${e}`);
			this.source.end();
		});
	}

	close() {
		if (connections.includes(this)) {
			connections.splice(connections.indexOf(this), 1);
			updateSessions();
		}
	}

	send(payload: Buffer) {
		this.source.write(Buffer.concat([Buffer.from([0x05]), payload]));
	}

	toString() {
		return `Connection(remoteAddress=${this.source.remoteAddress},session=${JSON.stringify(this.session)})`;
	}
}
