import { Session } from '../models/session';

export function shiftLeft(buffer: Buffer, bytes: number): Buffer {
	const value = Buffer.from(buffer.subarray(0, bytes));
	buffer.copy(buffer, 0, bytes);
	buffer.fill(0, buffer.length - bytes);
	return value;
}

export function compareSessions(session1: Session, sessions2: Session) {
	return (
		session1.maskIp === sessions2.maskIp &&
		session1.group === sessions2.group &&
		session1.identifier === sessions2.identifier &&
		session1.family === sessions2.family
	);
}
