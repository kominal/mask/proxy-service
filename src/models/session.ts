export interface Session {
	group: string;
	identifier: string;
	family: number;
	maskIp: string;
}
